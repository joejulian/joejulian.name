---
aliases: [/blog/securitymetrics-pci-testing-fails-again, /category/rants-amp-raves/securitymetrics-pci-testing-fails-again]
categories: [Rants & Raves]
date: '2012-09-25T17:46:41Z'
description: >
    They've done it again. @SecurityMetrics has irritated me with their insufficiently
    tested scan scripts and poor customer service. Is there someone else we should
    be using for PCI testing?
draft: false
tags:
slug: securitymetrics-pci-testing-fails-again
title: SecurityMetrics PCI testing fails again

---

They've done it again. [@SecurityMetrics](https://twitter.com/SecurityMetrics)
has irritated me with their insufficiently tested scan scripts and poor
customer service. Is there someone else we should be using for PCI testing?

This quarter they've found two "issues". One, javascript in html form values
that they're considering CSS vulerabilities. I don't know a single browser
that would execute the javascript in this, so I emailed them:

> In working to address the "CGI Generic Cross-Site Scripting"  tests I was
not able to cause this result to be interpreted as javascript. Is there a
browser in which this is interpreted that way?  
>  
>   <input type="hidden" name="ship_state"
value="--><script>alert(112)</script>">

Their response was less than stellar:

> Thank you for contacting SecurityMetrics Technical Support.  Our tests are
normally not run in browsers, and getting the results to show up in browsers
can be quite tiresome.  We normally perform these checks via a command line
terminal, as that is how the scanner is testing them.  I have never seen
cross-site scripting be a false-positive.

>

> It appears the page reported (<https://www.edwyse.com/homeapplication.php>)
has hidden fields that are reporting as vulnerable to cross-site scripting.
This is probably why it would be more difficult to have this vulnerability
appear via a browser.

>

> Once you feel you have removed the cross-site scripting issues a new scan
should be run as it will clarify that the issue has been resolved.

Well duh! Like I thought they had minions clicking through every page of every
website of every customer looking for vulnerabilities by hand. Any CSS
vulnerability check should, IMHO, filter out the places where scripts _canno_
_t_ be executed. Form input value fields are one of them.

My expectation, for the company that we pay to be the professionals in
security, is that they not only implement the tests but understand them. They
should _know_ how these potential vulnerabilities _could_ be a problem and be
able to communicate that effectively. They should ** __**_NOT_ be insisting
that their testing techniques are correct simply for lack of prior feedback.
This is especially false as I, personally, have provided feedback that has
pointed out false-positives in the past that have resulted in changes to the
tests.

Is there a better PCI compliance company that we should be using?

