---
aliases: [/blog/announcing-the-development-of-my-puppet-module-for-dynamic-glusterfs-volumes,
    /category/glusterfs/announcing-the-development-of-my-puppet-module-for-dynamic-glusterfs-volumes,
    /category/puppet/announcing-the-development-of-my-puppet-module-for-dynamic-glusterfs-volumes]
categories: [GlusterFS, Puppet]
date: '2013-07-23T13:45:00Z'
description: >
    'A couple times a year someone comes in to #gluster and thinks it would
    be awesome to ave a puppet module that you could define servers and the pupet
    module would automatically manage adding those servers as peers and dynamically
    changing volumes to utilize the new assets. They get inspired and start writing
    manifests... then they realize they actually have to get something completed and
    abandon that pie-in-the-sky philosophy and just use the very convenient cli.'
draft: false
tags:
slug: announcing-the-development-of-my-puppet-module-for-dynamic-glusterfs-volumes
title: Announcing the development of my Puppet module for dynamic GlusterFS volumes

---

A couple times a year someone comes in to #gluster and thinks it would be
awesome to ave a puppet module that you could define servers and the pupet
module would automatically manage adding those servers as peers and
dynamically changing volumes to utilize the new assets. They get inspired and
start writing manifests... then they realize they actually have to get
something completed and abandon that pie-in-the-sky philosophy and just use
the very convenient cli.

Two weeks ago, I was asked to speak at the Gluster Community Day in Portland
about managing GlusterFS with Puppet. I started putting slides together and
realized that I only had about 5 minutes of material. Make sure the packages
are installed, bricks are mounted, and the clients have fstab entries. Big
Whoop!

So, to have enough content, I had to start talking about the philisophical
idea of dynamically managing volumes (a concept I have argued is usually
overkill for most organizations). As long as I'm going to conceptualize about
it, why not write it?

So I did.

Announcing [joejulian-glusterfs](https://forge.gluster.org/joejulian-
glusterfs), a puppet module for managing dynamically changing GlusterFS
volumes.

Though not yet finished (I was really hoping to have it finished before this
talk, but I ran out of time), it should be ready for testing within a couple
of weeks.

