---
aliases: [/blog/setting-up-dogtag-pki-ca-certificate-manager, /category/howtos/setting-up-dogtag-pki-ca-certificate-manager]
categories: [Howtos]
date: '2013-05-13T01:54:53Z'
description: >
    Any service I can set up redundantly I, obviously, do. A couple years
    ago I set up redundant ldap servers. Last year my certificated expired and I generated
    new ones, not paying any attention to the serial id. Unfortunately, I generated
    two certificates with duplicate serials. sssd recognized that and failed to authenticate
    users via ldaps. My company's security requirements are relatively lax so I've
    just been authenticating over insecure ldap. That just annoys the hell out of
    me, though, and I've been wanting to fix it - but fix it the right way. Since
    I can't buy a CA from one of the big boys, I wanted to run my own CA server.&nbsp;
draft: false
tags:
    - dogtag
    - pki
    - certificate
    - authority
    - self-serve
slug: setting-up-dogtag-pki-ca-certificate-manager
title: Setting up Dogtag PKI CA Certificate Manager

---

Any service I can set up redundantly I, obviously, do. A couple years ago I
set up redundant ldap servers. Last year my certificated expired and I
generated new ones, not paying any attention to the serial id. Unfortunately,
I generated two certificates with duplicate serials. sssd recognized that and
failed to authenticate users via ldaps. My company's security requirements are
relatively lax so I've just been authenticating over insecure ldap. That just
annoys the hell out of me, though, and I've been wanting to fix it - but fix
it the right way. Since I can't buy a CA from one of the big boys, I wanted to
run my own CA server.

I found Dogtag, the Fedora version of Red Hat Security System, and tried
installing it on CentOS. Though it installs and configures, the web ui is
unusable. The Red Hat rpms were designed to be used with Red Hat Security
System. Without that, the ui templates are blank. So I tried again with
Fedora. Just a few minor hitches but all-in-all a fairly simple install.

## The hitches

I installed in Fedora 17 (I already had an image). Apparently there's some
dependencies that are not accounted for, mostly selinux related. To install,
first upgrade your packages.

    
    
    yum -y upgrade

Install semanage to satisfy an installation script requirement.

    
    
    yum -y install policycoreutils-python

## Install Dogtag

Install 389 Directory Service and Dogtag packages.

    
    
    yum -y install 389-ds dogtag-pki dogtag-pki-ca-theme dogtag-pki-common-theme \
      dogtag-pki-console-theme dogtag-pki-kra-theme dogtag-pki-ocsp-theme dogtag-pki-ra-theme \
      dogtag-pki-tks-theme dogtag-pki-tps-theme

Configure the directory service (I use keepcache in order to save the inf for
puppet).

    
    
    /usr/sbin/setup-ds.pl --keepcache
    
    ==============================================================================
    This program will set up the 389 Directory Server.
    
    It is recommended that you have "root" privilege to set up the software.
    Tips for using this  program:
      - Press "Enter" to choose the default and go to the next screen
      - Type "Control-B" or the word "back" then "Enter" to go back to the previous screen
      - Type "Control-C" to cancel the setup program
    
    Would you like to continue with set up? [yes]: yes
    
    ==============================================================================
    Your system has been scanned for potential problems, missing patches,
    etc.  The following output is a report of the items found that need to
    be addressed before running this software in a production
    environment.
    
    389 Directory Server system tuning analysis version 23-FEBRUARY-2012.
    
    NOTICE : System is x86_64-unknown-linux3.3.4-5.fc17.x86_64 (2 processors).
    
    NOTICE : The net.ipv4.tcp_keepalive_time is set to 7200000 milliseconds
    (120 minutes).  This may cause temporary server congestion from lost
    client connections.
    
    WARNING: There are only 1024 file descriptors (soft limit) available, which
    limit the number of simultaneous connections.  
    
    WARNING  : The warning messages above should be reviewed before proceeding.
    
    Would you like to continue? [no]: yes
    
    ==============================================================================
    Choose a setup type:
    
       1. Express
           Allows you to quickly set up the servers using the most
           common options and pre-defined defaults. Useful for quick
           evaluation of the products.
    
       2. Typical
           Allows you to specify common defaults and options.
    
       3. Custom
           Allows you to specify more advanced options. This is 
           recommended for experienced server administrators only.
    
    To accept the default shown in brackets, press the Enter key.
    
    Choose a setup type [2]: 
    
    ==============================================================================
    Enter the fully qualified domain name of the computer
    on which you're setting up server software. Using the form
    . Example: eros.example.com. To accept the default shown in brackets, press the Enter key. Warning: This step may take a few minutes if your DNS servers can not be reached or if DNS is not configured correctly. If you would rather not wait, hit Ctrl-C and run this program again with the following command line option to specify the hostname: General.FullMachineName=your.hostname.domain.name Computer name [pkica.domain.dom]: ============================================================================== The server must run as a specific user in a specific group. It is strongly recommended that this user should have no privileges on the computer (i.e. a non-root user). The setup procedure will give this user/group some permissions in specific paths/files to perform server-specific operations. If you have not yet created a user and group for the server, create this user and group using your native operating system utilities. System User [nobody]: System Group [nobody]: ============================================================================== The standard directory server network port number is 389. However, if you are not logged as the superuser, or port 389 is in use, the default value will be a random unused port number greater than 1024. If you want to use port 389, make sure that you are logged in as the superuser, that port 389 is not in use. Directory server network port [389]: ============================================================================== Each instance of a directory server requires a unique identifier. This identifier is used to name the various instance specific files and directories in the file system, as well as for other uses as a server instance identifier. Directory server identifier [pkica]: ============================================================================== The suffix is the root of your directory tree. The suffix must be a valid DN. It is recommended that you use the dc=domaincomponent suffix convention. For example, if your domain is example.com, you should use dc=example,dc=com for your suffix. Setup will create this initial suffix for you, but you may have more than one suffix. Use the directory server utilities to create additional suffixes. Suffix [dc=domain, dc=dom]: ============================================================================== Certain directory server operations require an administrative user. This user is referred to as the Directory Manager and typically has a bind Distinguished Name (DN) of cn=Directory Manager. You will also be prompted for the password for this user. The password must be at least 8 characters long, and contain no spaces. Press Control-B or type the word "back", then Enter to back up and start over. Directory Manager DN [cn=Directory Manager]: Password:  Password (confirm):  Your new DS instance 'pkica' was successfully created. Exiting . . . Log file is '/tmp/setupxZqVF7.log' 

Start up Dogtag.

    
    
    pkicreate -pki_instance_root=/var/lib    \
              -pki_instance_name=pki-ca          \
              -subsystem_type=ca                 \
              -agent_secure_port=9443            \
              -ee_secure_port=9444               \
              -ee_secure_client_auth_port=9446   \
              -admin_secure_port=9445            \
              -unsecure_port=9180                \
              -tomcat_server_port=9701           \
              -user=pkiuser                      \
              -group=pkiuser                     \
              -redirect conf=/etc/pki-ca         \
              -redirect logs=/var/log/pki-ca 
    
    
    
    PKI instance creation Utility ...
    
    Capturing installation information in /var/log/pki-ca-install.log
    [error] Failed setting selinux context pki_ca_port_t for 9180.  Port already defined otherwise.
    [error] Failed setting selinux context pki_ca_port_t for 9701.  Port already defined otherwise.
    [error] Failed setting selinux context pki_ca_port_t for 9443.  Port already defined otherwise.
    [error] Failed setting selinux context pki_ca_port_t for 9444.  Port already defined otherwise.
    [error] Failed setting selinux context pki_ca_port_t for 9446.  Port already defined otherwise.
    [error] Failed setting selinux context pki_ca_port_t for 9445.  Port already defined otherwise.
    
    PKI instance creation completed ...
    
    Installation information recorded in /var/log/pki-ca-install.log.
    Before proceeding with the configuration, make sure 
    the firewall settings of this machine permit proper 
    access to this subsystem. 
    
    Please start the configuration by accessing:
    
    https://pkica.domain.dom:9445/ca/admin/console/config/login?pin=5mcwyshTcFRqc23rMg6J
    
    After configuration, the server can be operated by the command:
    
        /bin/systemctl restart pki-cad@pki-ca.service
    

Open your iptables ports 9180/tcp, 9701/tcp, 9443-9446/tcp.

Begin configuring Dogtag using Firefox (not Chrome!). As it says, open the
link. You should be able to safely step through all the defaults to the end.
Set the same password on the directory service configuration. Probably add
your own admin data.

