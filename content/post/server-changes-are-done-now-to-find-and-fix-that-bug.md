---
aliases: [/blog/server-changes-are-done-now-to-find-and-fix-that-bug]
categories: []
date: '2010-01-24T18:49:53Z'
description: >
    Thanks for helping track down this bug. I spent the last several nights
    getting every little piece ready for transitioning my co-lo box from Fedora to
    CentOS. Last night I made the switch and tested all my customers sites to make
    sure they were still working.
draft: false
tags:
    - migrate
    - fedora
    - centos
    - Uncategorized
slug: server-changes-are-done-now-to-find-and-fix-that-bug
title: Server changes are done, now to find and fix that bug...

---

Thanks for helping track down this bug. I spent the last several nights
getting every little piece ready for transitioning my co-lo box from Fedora to
CentOS. Last night I made the switch and tested all my customers sites to make
sure they were still working.

Now, thanks to your feedback, I should be posting a new revision fixing that
missing script bug! (Right after I have some breakfast.)

