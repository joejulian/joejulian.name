---
aliases: [/blog/store-background-music-sucks-but-not-anymore]
categories: []
date: '2012-05-16T11:56:55Z'
description: >
    I just finished building a cute little pianobar player for our stores
    at Ed Wyse on equipment that was being retired. With these, no more staticy radio,
    listening to the same stack of cd's over and over again, or employee's ipods (sorry
    for the store that had to listen to mine, funny that you didn't notice Pac-Man
    Fever for two days though).
draft: false
tags:
slug: store-background-music-sucks-but-not-anymore
title: Store background music sucks - but not anymore.

---

![Wyse Winterm](/media/uploads/images/wyse_winterm.jpg)I just finished
building a cute little [pianobar](http://6xq.net/projects/pianobar/ "pianobar
pandora player") player for our stores at [Ed Wyse](http://www.edwyse.com "Ed
Wyse Beauty Supply") on equipment that was being retired. With these, no more
staticy radio, listening to the same stack of cd's over and over again, or
employee's ipods (sorry for the store that had to listen to mine, funny that
you didn't notice [Pac-Man Fever](http://www.youtube.com/watch?v=kBdAPkk8yMM
"Pac Man Fever on Youtube") for two days though).

It's running [ttylinux](http://ttylinux.net/ "ttylinux") for the most part.
Several of the dependencies I pulled from fedora rpms or rpmfusion's, and I
compiled pianobar on a 32 bit fedora vm. There's supposed to be a way to build
this all from within ttylinux, but I was up against a deadline and had to just
make it work.

I decided on Pandora for our music source because I knew about the pianobar
player that would work from a command line and doesn't require flash. Since
we're paying for Pandora One, there shouldn't be any issues with commercials.
The GM can access the account from anywhere, and can manage the playlist. When
special events come, she can even set up different playlists and we can switch
to those just on the correct days.

Since it's going over an overhead speaker system, I had to figure out how to
join the stereo output from Pandora into a mono signal. Here's the foo that
made that happen (I couldn't find it when I googled).

asound.conf:

    
    
    pcm.!default {  
        type plug  
        slave.pcm "mono"  
    }  
    pcm.mono {  
        type route  
        slave {  
            pcm mixer  
            channels 2  
        }   
        ttable.0.0 0.5  
        ttable.0.1 0.5  
        ttable.1.0 0.5  
        ttable.1.1 0.5  
    }  
    pcm.mixer {  
        type hw  
        card 0  
    }

The rest was fairly simple. I wrapped pianobar in an init script and it's off
and running.

Next, build an api so I can control all the stores from a centralized web
page, and report playlists.

