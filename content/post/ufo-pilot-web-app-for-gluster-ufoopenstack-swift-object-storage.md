---
aliases: [/blog/ufo-pilot-web-app-for-gluster-ufoopenstack-swift-object-storage, /category/glusterfs/ufo-pilot-web-app-for-gluster-ufoopenstack-swift-object-storage,
    /category/ipv6/ufo-pilot-web-app-for-gluster-ufoopenstack-swift-object-storage,
    /category/openstack/ufo-pilot-web-app-for-gluster-ufoopenstack-swift-object-storage]
categories: [GlusterFS, IPv6, OpenStack]
date: '2012-10-24T07:11:04Z'
description: >
    I'm happy to announce the availability of UFO Pilot, an HTML5 web app
    for accessing a Gluster UFO/OpenStack Swift object store from your Android or
    iOS mobile device.
draft: false
tags:
    - glusterfs
    - ios
    - html5
    - javascript
    - ufo
    - openstack
    - swift
    - redhat
    - storage
slug: ufo-pilot-web-app-for-gluster-ufoopenstack-swift-object-storage
title: UFO Pilot - web app for Gluster UFO/OpenStack Swift Object Storage

---

I'm happy to announce the availability of UFO Pilot, an HTML5 web app for
accessing a Gluster UFO/OpenStack Swift object store from your Android or iOS
mobile device.

![Login
Screen](http://joejulian.name:8080/v1/AUTH_joe/public/screenshot-1351051592430.png)

![screenshot](http://joejulian.name:8080/v1/AUTH_joe/public/screenshot-1351052034165.png)

Feel free to download it from [Github](https://github.com/joejulian/ufopilot)
and offer your feedback.

As an aside, Gluster UFO and OpenStack Swift (pretty much one and the same),
does not support IPv6. This is due to a deficiency in the socket library that
was used. I was pretty disappointed when I discovered that.

To any graphic artists out there, I would like some help making a decent title
bar like the one I cobbled together from a pre-existing header for the fine
folks at Red Hat:

![Red Hat
Screenshot](http://joejulian.name:8080/v1/AUTH_joe/public/screenshot-1351047642888.png)

