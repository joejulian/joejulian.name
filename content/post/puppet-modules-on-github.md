---
aliases: [/blog/puppet-modules-on-github, /category/puppet/puppet-modules-on-github]
categories: [Puppet]
date: '2012-04-21T08:09:59Z'
description: >
    In case anyone's interested, I have some puppet modules I've created.
draft: false
tags:
slug: puppet-modules-on-github
title: Puppet Modules

---

In case anyone's interested, I have some puppet modules I've created.

Almost every other puppet module out there is ubuntu-centric and there are
very few geared toward RHEL/CentOS/Fedora. Mine are, though I've tried to add
structure to allow other distros to expand on what I've created. I've started
putting them up at [github](https://github.com/joejulian). Please add other
distro support and send me a pull request.

At least one other person's using one of them. I'm glad someone other than
myself is finding them useful.

