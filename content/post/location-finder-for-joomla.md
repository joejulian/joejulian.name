---
aliases: [/blog/location-finder-for-joomla]
categories: []
date: '2009-12-22T22:29:20Z'
description: >
    '[caption id="attachment_6" align="alignleft" width="150" caption="Screenshot"][/caption]'
draft: false
tags:
    - Google
    - Maps
    - Store
    - Locations
    - component
    - Location
    - Location Finder
    - v3
    - Joomla!
    - Plugins
slug: location-finder-for-joomla
title: Location Finder for Joomla!

---

[caption id="attachment_6" align="alignleft" width="150"
caption="Screenshot"][![Screenshot of LocationFinder for
Joomla!](http://joejulian.name/wp-content/uploads/2009/12/Screenshot-
locationfinder-Firefox-150x150.png)](http://joejulian.name/wp-
content/uploads/2009/12/Screenshot-locationfinder-Firefox.png)[/caption]

Location Finder for Joomla! is a component that allows your users to search
for the closest location of your business and get driving directions.
Implemented using Google Maps' API v3, this is very quick to use, and easy to
implement.

To install, [download the current version](http://joejulian.name/?page_id=11)
and install using Joomla!'s installer. Select Location Finder from the
Components menu and start adding your locations. Make sure you click the
"Lookup" button to retrieve the coordinates from Google. The description is
displayed in an info box over the map when someone clicks on the location.

Add it to your menu, and you're ready to go.
