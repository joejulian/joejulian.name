---
aliases: [/blog/attending-red-hat-summit, /category/glusterfs/attending-red-hat-summit]
categories: [GlusterFS]
date: '2012-06-15T00:40:16Z'
description: >
    It was just finalized. I'll be attending Red Hat Summit on June 27th
    and 28th and sharing my GlusterFS knowledge. If you're there, please come and
    find me and say Hi. I'll be monitoring Twitter and Google+.
draft: false
tags:
    - glusterfs
    - red
    - hat
slug: attending-red-hat-summit
title: I'll be attending Red Hat Summit

---

It was just finalized. I'll be attending [Red Hat
Summit](http://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=0CGUQFjAA&url=http%3A%2F%2Fwww.redhat.com%2Fsummit%2F&ei=zIPaT9i9CuLM2AWnp9SCBg&usg
=AFQjCNHR1mLO4UTLGJCLSyD-EU1yTHYphQ&sig2=AO1lVaWAXUyj4vcTVHTXSw) on June 27th
and 28th and sharing my [GlusterFS](http://www.gluster.org) knowledge. If
you're there, please come and find me and say Hi. I'll be monitoring
[Twitter](https://twitter.com/#!/JoeCyberGuru) and
[Google+](https://plus.google.com/113457525690313682370).

From the Red Hat Summit site:

## About Red Hat Summit and JBoss World

The Red Hat Summit and JBoss World are the premier open source technology
events to showcase the latest and greatest in cloud computing, platform,
virtualization, middleware, storage, and systems management technologies.

By attending, you'll gain the best knowledge in the industry through:

  * technical and business sessions
  * hands-on labs and demos
  * customer panels and presentations
  * visionary keynotes from industry leaders
  * networking opportunities and events
  * direct collaboration with Red Hat and JBoss engineers

