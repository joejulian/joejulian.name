---
aliases: [/blog/centos-6-upstart-overrides-how-to-make-something-happen-before-something-else,
    /category/howtos/centos-6-upstart-overrides-how-to-make-something-happen-before-something-else]
categories: [Howtos]
date: '2013-07-01T20:33:48Z'
description: >
    I have generic users that are logged in automatically since they're not
    allowed user prefs anyway, and it would be more of a hindrance in their environment.
draft: false
tags:
    - centos
    - debian
    - ubuntu
    - upstart
slug: centos-6-upstart-overrides-how-to-make-something-happen-before-something-else
title: 'CentOS 6 upstart overrides: How to make something happen before something
    else'

---

I have generic users that are logged in automatically since they're not
allowed user prefs anyway, and it would be more of a hindrance in their
environment.

I use puppet to manage a staging directory that has the home directory as I
want it to be, and use tmpfs for the user's actual $HOME. In order for this to
work, the staged default home needs to be copied onto the tmpfs mount before
Xwindows starts.

All this means that I can't do it in rc.local as that's run simultaneous to
the prefdm upstart job, creating a race condition.

To resolve the problem, I learned about upstart override configuration. It
turns out you can create a <jobname>.override and override any stanza in the
packaged upstart job configuration. In my case I created the following:

/etc/prefdm.override  

    
    
    # homedir - sets up the home directory of the auto-logged-in user
    #
    
    pre-start script
    	/usr/bin/puppet agent --onetime --no-daemonize >/var/log/prestart.log
    	/usr/bin/rsync -av /home/thinguest.orig/ /home/thinguest/ >>/var/log/prestart.log
    	/sbin/restorecon -rv /home/thinguest >> /var/log/prestart.log
    end script

This overrides the non-existent pre-start stanza in prefdm.conf with that
script, ensuring those things are done before /etc/X11/prefdm -nodaemon is
executed.

This is actually true, of course, for all the EL6 based distros, and also is
true for Ubuntu/Debian.

