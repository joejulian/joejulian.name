---
aliases: [/blog/2013-cascadia-it-conference, /category/glusterfs/2013-cascadia-it-conference,
    /category/openstack/2013-cascadia-it-conference, /category/puppet/2013-cascadia-it-conference]
categories: [GlusterFS, OpenStack, Puppet]
date: '2012-11-11T01:43:16Z'
description: >
    The Cascadia IT Conference is looking for sponsors and proposals for
    talks, tutorials and panels.
draft: false
tags:
    - lopsa
    - sasag
    - glusterfs
slug: 2013-cascadia-it-conference
title: 2013 Cascadia IT Conference

---

The [Cascadia IT Conference](http://casitconf.org/casitconf13/) is looking for
sponsors and proposals for talks, tutorials and panels.

> Lets get this thing started! Cascadia IT Conference 2013 is March 15,16.
Submit your proposals now. Earlier is better
[casitconf.org/casitconf13/cf…](http://t.co/UDLZr7mL
"http://casitconf.org/casitconf13/cfp/")

>

> -- Cascadia IT Conf (@casitconf) [November 8,
2012](https://twitter.com/casitconf/status/266410103228489729)

What _**I**_ would really like to do, if any hardware folks would like to
participate, is to put together a hands-on GlusterFS workshop.

The way I would envision it would be a coffee-shop style area with laptops,
chromebooks, thin clients, or even tablets available. Myself, and one or two
other people, would be available to offer assistance and tutelage. People
would sit down and take a device. Be given a few freshly installed VMs and
install and configure their first GlusterFS volume.

Participation from an IaaS provider would be beneficial as well, perhaps my
friends at Rackspace or, since they're right here in town, Amazon might like
to help with that?

If you have contacts that might be able to help make this idea happen, please
put them in touch with [me@joejulian.name](mailto:me@joejulian.name)

