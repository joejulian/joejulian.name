---
aliases: [/blog/project-page-is-up]
categories: []
date: '2010-01-28T07:40:27Z'
description: >
    I've installed a project page at http://joomla.joejulian.name/bzr
draft: false
tags:
    - Wiki
    - Project
    - bzr
    - Forum
    - Repository
    - Location Finder
    - Bug
    - Redmine
    - Joomla!
    - Plugins 
slug: project-page-is-up
title: Project page is up

---

I've installed a project page at <http://joomla.joejulian.name/bzr>

There you can see the change logs, browse the file revisions, access the
[bazaar](http://bazaar.canonical.com) repo, track bugs, and more...

