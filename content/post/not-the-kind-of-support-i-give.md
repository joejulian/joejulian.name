---
aliases: [/blog/not-the-kind-of-support-i-give, /category/rants-amp-raves/not-the-kind-of-support-i-give]
categories: [Rants & Raves]
date: '2011-09-05T00:56:33Z'
description: >
    'I had a problem where /lib/dri/r600_dri.so was left behind on a x86_64
    system during an upgrade from F14 to F15 using the Anaconda DVD upgrade. Not noticing
    this was a 32 bit app, when I couldn''t figure out why the error message was showing
    up from a line number that didn''t correlate with the source code, I played around
    with using rawhide packages and seeing if that changed anything. When it didn''t,
    I asked a question on #fedora. Now I know that both #fedora and #centos are run
    by people with no manners and god complexes, but I did it anyway.'
draft: false
tags:
    - rants
    - amp
    - raves
slug: not-the-kind-of-support-i-give
title: Not the kind of support I give...

---

I had a problem where /lib/dri/r600_dri.so was left behind on a x86_64 system
during an upgrade from F14 to F15 using the Anaconda DVD upgrade. Not noticing
this was a 32 bit app, when I couldn't figure out why the error message was
showing up from a line number that didn't correlate with the source code, I
played around with using rawhide packages and seeing if that changed anything.
When it didn't, I asked a question on #fedora. Now I know that both #fedora
and #centos are run by people with no manners and god complexes, but I did it
anyway.

fenrus02, after telling me off for mixing distro versions despite the fact
that my question had already explained that the version wasn't even being
read, told me that his post-upgrade page would fix this problem and insisted,
despite evidence to the contrary, that I was wrong.

So let's analyze his method and see where he expected this problem would have
become evident (he won't say how he expected this to work).

0\. Change runlevel to 3 ( <http://fedorasolved.org/post-install-
solutions/runlevel> ) (Use the grub method) and login as root.  You can use
"cnetworkmanager" or "nmcli" if you need to get networking started without the
GUI.

Irrelevant. Has nothing to do with finding 32 bit libraries with no rpmdb
Package data.

1\. Update your system:

    
    
    # **rm /var/lib/rpm/__db.00?;**

Delete lock files. Won't find files in /lib/dri that don't have Packages

    
    
    # **yum clean all;**

Delete yum cache. Won't find files in /lib/dri that don't have Packages

    
    
    # **yum-complete-transaction;**

If there were any pending yum installs, perhaps mesa-dri-drivers.i686, then
this could work. Since there weren't, no.

    
    
    # **yum update --skip-broken;**

Since mesa-dri-drivers.i686 wasn't installed, this update will not affect that
file.

    
    
    # **rpm -a --setugids; rpm -a --setperms;**

Resetting file permissions will, again, not version library files that aren't
Packaged. In fact, even their gids and permissions won't get reset.

    
    
    # **yum install @core @base;**

mesa-dri-drivers.i686 is in neither @core or @base

2\. (Optional) Remove old packages from cache directories

    
    
    # **DIST=$(rpm --eval '%{dist}'); find /var/cache/yum/ -type f -name \*.rpm |grep -v $DIST |xargs rm -f;**

Delete old yum cache files from other distributions. Won't fix the library
issue.

If you have yum-plugin-local installed, you will want to free up spaced it
used:

    
    
    # **DIST=$(rpm --eval '%{dist}'); find /var/lib/yum/plugins/local/ -type f -name \*.rpm |grep -v $DIST |xargs rm -f;**

Deleting these files also not going to affect /lib/dri/*

3\. (Optional) Install basic components you would have from a new install:
You likely want to include the desktop of your choice as well, such as
**@gnome-desktop** or **@kde-desktop** or **@xfce-desktop** or **@lxde-
desktop**

    
    
    # **yum install @base-x @base @core @fonts @input-methods @admin-tools @dial-up @hardware-support @printing fpaste memtest86+** _@gnome-desktop_ ;

mesa-dri-drivers.i686 is not in any of those groups.

4\. Correct labels and reboot:  (This command takes about 11 minutes to run on
my hardware.  Yours may be quicker or slower.  Give it time to complete.)

    
    
    # **fixfiles -R -a restore; reboot;**

The above command only does the files yum/rpm installed. If you would rather
relabel all files on the system, use this instead: (Note: It may take longer
on reboot)

    
    
    # **fixfiles onboot; reboot;**

Resetting the selinux context is also not going to update the i686 version of
those libraries.

5\. Newer versions of yum include this command.  If your version does not yet
support it, skip this step:  (yum-3.2.28-1 is known to work)

    
    
    # **yum distribution-synchronization --disablepresto;**

distro-sync does not perform operations on "local packages". That may be what
these files are considered since they have no package. The yum python scripts
don't do anything that would look at an existent directory and install the
package to manage it. This will have no effect.

6\. Login again with runlevel 3 and as root.  Install yum-utils and print out
a list of all the packages that need review:  (This will print out packages
that have dependency problems as well as packages that are no longer found in
your configured repos and any duplicate packages you might have.)

    
    
    # **rpm -Va --nofiles --nodigest;**

The package doesn't exist in the rpm database, so this will have no effect.

    
    
    # **yum install yum-utils;**

Obviously installs yum-utils, no effect.

    
    
    # **package-cleanup --problems;**

package-cleanup works through the rpm database package table, so it will not
even look at the dri libraries.

    
    
    # **package-cleanup --orphans;**

ditto

    
    
    # **package-cleanup --dupes;**

ditto

    
    
    (optionally locate packages you may not need any longer)
    # **package-cleanup --leaves;**

ditto

7\. In most cases, you should remove the versions listed above and install the
current distribution version of the package instead if you still use it.

8\. Create a list of all the files that have verify concerns:

    
    
    # **. /etc/sysconfig/prelink && /usr/sbin/prelink -av $PRELINK_OPTS >> /var/log/prelink/prelink.log 2>&1;**

This is where I'm guessing he thought it would show up. If there was a library
linkage to the file that wasn't there, I could see getting an error about a
missing library. But since it was there, there are no errors associated with
the mesa-dri-drivers. No effect.

    
    
    # **/sbin/ldconfig;**
    # **rpm -Va > /tmp/rpm-Va.txt 2>&1;**

I don't care how many times you walk the rpm database, if the package isn't
there, it's not going to check the files that aren't associated with it. No
effect.

9\. Using the above, create a list of non-configuration files that need
review:

    
    
    # **egrep -v '^.{9} c /' /tmp/rpm-Va.txt > /tmp/URGENT-REVIEW.txt;**

Won't be here for the reason stated above.

10\. Using the above, create a list of configuration files that need review:

    
    
    # **egrep '^.{9} c /' /tmp/rpm-Va.txt > /tmp/REVIEW-CONFIGS.txt;**

Won't be here for the reason stated above.

11\. Review the lists above. Consult "man rpm" under the VERIFY section for
the meaning of the first column.  You can usually ignore lines if they have a
"prelink: /usr/bin/somefile: at least one of file's dependencies has changed
since prelinking" type message next to it.

12\. Locate your changed config files and manually merge the changes (If you
have yum-plugin-merge-conf installed, you can use it here to assist as well):

    
    
    # **yum install rpmconf; rpmconf -a;**

rpmconf walks the rpm database Packages table. It's still not in there so this
will have no effect.

    
    
    # **find /etc /var -name '*.rpm?*' > /tmp/REVIEW-OBSOLETE-CONFIGS.txt;**

It's not in /etc or /var so that's going to be irrelevant right off the bat.

## Summary

I was told several times that this was the end-all-be-all answer to the
problem I asked about. I argued as it's clear by reading through the list of
commands that it would have no effect. When I was chastised and told that I
could, "decide to live with misery for the next few months while you sort it
out yourself of course" I was offended.

DiscordianUK chimed in later saying that F17 packages were supported in
fedora-qa which, of course, had nothing to do with my question. And I was
accused of acting "entitled" by EvilBob. I don't feel it's inappropriate to
feel entitled to common courtesy.

If you feel that someone has done something foolish, but that foolish thing
isn't what he's asking about you can be rude and hostile, or you can ignore
the question, or you could actually read what was asked and respond to it. I
don't feel rude and hostile is worth anybody's time.

