---
aliases: [/blog/using-alex-gorbatchevs-syntaxhighlighter-with-mezzanine, /category/howtos/using-alex-gorbatchevs-syntaxhighlighter-with-mezzanine,
    /category/mezzanine/using-alex-gorbatchevs-syntaxhighlighter-with-mezzanine]
categories: [Howtos, Mezzanine]
date: '2012-06-29T17:45:45Z'
description: >
    One tool I use on this site is Alex Gorbatchev's javascript based syntax
    highlighter. It's a really cool tool that makes it really easy to highlight syntax
    with very simple &lt;pre&gt; markup. To make it work with Mezzanine was pretty
    simple. I added the scripts to my footer_scripts.html template, ie.
draft: false
tags:
    - syntaxhighlighter
    - mezzaine
slug: using-alex-gorbatchevs-syntaxhighlighter-with-mezzanine
title: Using Alex Gorbatchev's SyntaxHighlighter with Mezzanine

---

One tool I use on this site is Alex Gorbatchev's javascript based syntax
highlighter. It's a really cool tool that makes it really easy to highlight
syntax with very simple <pre> markup. To make it work with Mezzanine was
pretty simple. I added the scripts to my footer_scripts.html template, ie.

    
    
    <!-- code syntax highlighter -->
    <script src="http://alexgorbatchev.com/pub/sh/current/scripts/shCore.js" type="text/javascript"></script>
    <script src="http://alexgorbatchev.com/pub/sh/current/scripts/shBrushPython.js" type="text/javascript"></script>
    <script src="http://alexgorbatchev.com/pub/sh/current/scripts/shBrushRuby.js" type="text/javascript"></script>
    <script src="http://alexgorbatchev.com/pub/sh/current/scripts/shBrushCpp.js" type="text/javascript"></script>
    <script src="http://alexgorbatchev.com/pub/sh/current/scripts/shBrushBash.js" type="text/javascript"></script>
    <script src="http://alexgorbatchev.com/pub/sh/current/scripts/shBrushXml.js" type="text/javascript"></script>
    <script src="http://alexgorbatchev.com/pub/sh/current/scripts/shAutoloader.js" type="text/javascript"></script>
    

To allow me to actually add the <pre class="brush:brushtype"> markup using
tinymce, in local_settings.py, I added

    
    
    TINYMCE_SETUP_JS = "js/tinymce_setup.js"
    

which reads from the static path.

Finally, in $static/js/tinymce_setup.js I changed the definition of pre in
extended_valid_elements from

    
    
    pre[style]
    

to

    
    
    pre[style|class]
    

