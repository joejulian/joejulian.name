---
aliases: [/blog/location-finder-092beta1, /category/rants-amp-raves/location-finder-092beta1]
categories: [Rants & Raves]
date: '2010-01-19T09:51:31Z'
description: >
    Over 300 downloads so far, and not one bit of feedback. If you download
    the beta and it fixes the problem where you search for a location (that you know
    is there) and nothing comes up, let me know! I need your feedback!
draft: false
tags:
    - feedback
    - testing
    - Location
    - Location Finder
    - beta
    - Joomla!
    - Plugins
    - "Rants & Raves"
slug: location-finder-092beta1
title: Location Finder 0.9.2beta1

---

Over 300 downloads so far, and not one bit of feedback. If you download the
beta and it fixes the problem where you search for a location (that you know
is there) and nothing comes up, let me know! I need your feedback!

Thanks.

