---
aliases: [/blog/review-msi-gx70, /category/rants-amp-raves/review-msi-gx70]
categories: [Rants & Raves]
date: '2013-11-01T19:30:21.507Z'
description: >
    To anyone not aware, I'm a Fedora user. I've been using Fedora for my
    desktop machines since my 16MHz 386. There's seldom any severe issues, but occasionally
    there are. I abandoned Nvidia due to their abysmal support for Linux.
draft: false
tags:
slug: review-msi-gx70
title: 'Review: MSI GX70'

---

To anyone not aware, I'm a Fedora user. I've been using Fedora for my desktop
machines since my 16MHz 386. There's seldom any severe issues, but
occasionally there are. I abandoned Nvidia due to their abysmal support for
Linux.

Recently I replaced my aging (but bulletproof) Fujitsu. Six years old and the
damn thing would just not quit. The only problem I ever have is that I can't
plug it in to a projector and have it work. Since I'm doing a lot more
speaking, this has become a serious issue.

I shopped for something I could have some fun with in my down time (a gaming
laptop) yet one that I could use on a 6 hour flight on one battery. I chose
the MSI GX70, in part after playing with it at PAX. I can play most games with
everything turned all the way up and it looks and feels beautiful. It's super
heavy and huge, but that just keeps me young, right? :D

The biggest problem I've encountered so far is that the open-source drivers
just don't work. The blob worked on Fedora 19, but after upgrading to Fedora
20, it claimed that my video cards (yes, plural) were not supported. The open
source drivers [fail to initialize the ATI Radeon HD
8970M](https://bugzilla.redhat.com/show_bug.cgi?id=1021779). Like the prior
problems with nouveau drivers, bug reports seem to remain ignored. I don't
know if the Fedora rpm maintainer just has no time, or ... ?

I've tried the #radeon irc channel and the closest I got to a response was the
speculation that there was a memory problem. Though I don't rule that out
entirely, there was no problem with the blob before I upgraded and there's no
problem in Windows (I keep it around for games still).

For grins, since I knew there was no way they were going to actually help me,
I filed a support reqest with MSI. That's not to say that MSI support sucks.
I've never files a support request with them in the past so I have no opinion
whatsoever. I just know that asking a bunch of people making $5/hr in
whichever country is popular for online support these days to manage getting a
hardware engineer to take a look at a driver is next to impossible. As
expected the answer was less than stellar.

> MSI Tech. | 11/01/2013 | Dear Customer, we can only support the OS that was
originally installed on the notebook and can not provide assistance for
problems with 3rd party OS, restore the original factory image if you have
problems with the original OS we can then assist you. | [
](http://service.msicomputer.com/msi_user/tsdform_p.aspx?case_id=13024399&email=joe@julianfamily.org)  
> --->--|---|---  
> MSI Tech. | 11/01/2013 | Please verify if the issue exists on Windows or not
since we don't offer support with Linux, thank you.  
  
This is not the '80s though and I feel the manufacturers need to start taking
a greater interest in ensuring their hardware is supported by Linux. I've
filed a followup request asking them to get an engineer involved in taking a
look at my bug. Let's see if they step up.

