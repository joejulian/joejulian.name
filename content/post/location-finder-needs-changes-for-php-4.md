---
aliases: [/blog/location-finder-needs-changes-for-php-4]
categories: []
date: '2010-01-25T16:06:51Z'
description: >
    PHP 5 changed and retired the old method names for DOMDocument from PHP
    4. If you're running PHP 4, bookmark my site and check back. I need to setup a
    test environment and make the callback script work with PHP 4.
draft: false
tags:
    - Location Finder
    - php4
    - component
    - Joomla!
    - Plugins
slug: location-finder-needs-changes-for-php-4
title: Location Finder needs changes for PHP 4

---

PHP 5 changed and retired the old method names for DOMDocument from PHP 4. If
you're running PHP 4, bookmark my site and check back. I need to setup a test
environment and make the callback script work with PHP 4.

****UPDATE**

I've put a php4 untested patch up (beta 7). If you're using php 4.2.0 or
greater, this needs testing.

****UPDATE 2**

Still not completely tested on php4, but mostly... If using php4 this should
work. Let me know.

