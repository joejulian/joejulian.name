---
aliases: [/blog/fedora-19-with-legacy-glusterfs-33, /category/glusterfs/fedora-19-with-legacy-glusterfs-33,
    /category/rants-amp-raves/fedora-19-with-legacy-glusterfs-33]
categories: [GlusterFS, Rants & Raves]
date: '2013-06-26T19:01:04Z'
description: >
    I went into this expecting problems. Fedora 19 ships with GlusterFS 3.4.0
    beta and I'm using GlusterFS 3.3 in production. I expected that I would have to
    downgrade my Fedora packages so I could use my volumes. I expected problems.
draft: false
tags:
    - glusterfs
    - fedora
    - 3.3
    - 3.4
slug: fedora-19-with-legacy-glusterfs-33
title: Fedora 19 with legacy GlusterFS 3.3

---

I went into this expecting problems. Fedora 19 ships with GlusterFS 3.4.0 beta
and I'm using GlusterFS 3.3 in production. I expected that I would have to
downgrade my Fedora packages so I could use my volumes. I expected problems.

What I didn't expect was that my wishes for the last three releases would be
realized in 3.4. RPC compatibility!

I started futzing around with downgrading and encountering problems... Doesn't
really matter what those problems were because that was a complete waste of
time.

I reinstalled the latest beta packages and mounted my volumes.

I was able to be a member of the peer group (my Fedora box is a member for CLI
convenience only).

Everything just worked.

![Happy
Dance](https://lh6.googleusercontent.com/-1dDTYdSM8uQ/T-Necp_PSiI/AAAAAAABP1Q/GGHBC6eacDk/s256-no
/snoopy-dance-5317_preview.gif)

