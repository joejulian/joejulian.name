---
aliases: [/blog/pci-security-they-dont-even-have-to-hack-in, /category/rants-amp-raves/pci-security-they-dont-even-have-to-hack-in]
categories: [Rants & Raves]
date: '2013-04-30T06:30:43.107Z'
description: >
    Yesterday, in the middle of our business day, we suddenly could not run
    credit cards from our corporate office. The response code we received back indicated
    a bad terminal id associated with our merchant account.
draft: false
tags:
slug: pci-security-they-dont-even-have-to-hack-in
title: PCI security? They don't even have to hack in...

---

Yesterday, in the middle of our business day, we suddenly could not run credit
cards from our corporate office. The response code we received back indicated
a bad terminal id associated with our merchant account.

Our merchant bank is going through a transition to a different processor and
I've been dragging my feet to make the change as our custom software needed
some things fixed before I was willing to make the attempt (besides having 20
other irons in the fire that are all needing some level of resolution before I
fix something that's not broken). This made me fear that they had taken some
action to force the transition, or had just made some mistake. I was not a
happy camper.

I called our "Relationship Manager" (I'll call him Frank), got voicemail.
Called his cell phone, got voicemail. *sigh* I had a doctor's appointment in
half an hour, so I grabbed my laptop and left, all the while playing through
alternatives in my mind so I could recover from this disaster with a minimal
impact.

As I drove to the appointment I went through several possible recovery
scenarios with our company president. If Frank was able to call back with a
resolution within a half an hour, we could wait. Otherwise we would have to
punt to one of my other options.

Frank called. He didn't know what had happened but assured me it had nothing
to do with the transition. He would check in to it and call me back.

I was called in to my appointment from the waiting room.

About 20 minutes later, as we're rapping up discussion about how to try to get
my cholesterol levels where they're supposed to be, I was saved by a phone
call. Frank called back to tell me that our TID had been suspended due to
fraudulent activity. Apparently "carders" will randomly throw together numbers
in attempts to find valid merchant numbers + terminal ids.

Here's all that's needed to hack a VID is to acquire some information that's
common to all accounts, and a series of about 20 numeric characters, many of
which can be pre-determined. Using a standard distributed virus hack, every
possible combination could be tried very quickly with no single IP address to
flag for failures. If they phished the merchant it then it was even easier.

This all brings to mind a quesiton. In a world where we're required to use ssl
encryption for user transactions, why are we still using these short merchant
it/terminal id numbers? Why isn't this using cryptographically signed and
authenticated certificates?

