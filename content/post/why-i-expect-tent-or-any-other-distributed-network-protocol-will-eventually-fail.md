---
aliases: [/blog/why-i-expect-tent-or-any-other-distributed-network-protocol-will-eventually-fail]
categories: []
date: '2012-08-23T23:01:12Z'
description: >
    Back at the end of 2003 I was toying with Gnutella's distributed peer-to-peer
    network and wanted to host a peer lookup server myself. I tried several cache
    servers but most of them didn't support the newer protocol and would frequently
    fail. I wrote my own. It worked very well and was quickly becoming the most popular
    of it's kind until some commercial software developers wrote horrible client software
    to make use of the network.
draft: false
tags:
slug: why-i-expect-tent-or-any-other-distributed-network-protocol-will-eventually-fail
title: Why I expect Tent, or any other distributed network protocol, will eventually
    fail

---

Back at the end of 2003 I was toying with Gnutella's distributed peer-to-peer
network and wanted to host a peer lookup server myself. I tried several cache
servers but most of them didn't support the newer protocol and would
frequently fail. [I wrote my own](http://gwcii.sourceforge.net/). It worked
very well and was quickly becoming the most popular of it's kind until some
commercial software developers wrote horrible client software to make use of
the network.

These poorly written clients had no sanity checks to ensure that they didn't
spam the network and they quickly began overloading every cache server. With
loads and bandwidth limits being exceeded, shared hosting providers started
killing cache servers. People that were running servers on their own hosts
stopped. The network fell apart. Still, even after the caches expired the
removed servers, these poorly written clients continued to hammer domains.
People actually abandoned their domain names just to get away from the spam.

I still have several hundred requests for my cache that hasn't been running
since mid 2004.

That's what I would be afraid of happinging with Tent. If it becomes popular
enough, the network loads may exceed the user's willingness to participate.
That would seem to doom any peer distributed networking protocol.

If you do choose to participate, I would suggest using a throw-away domain and
using Tor so that if you do choose to cease participating you can get away
cleanly.  
  

