---
aliases: [/blog/are-glusterfs-competitors-pulling-a-microsoft, /category/glusterfs/are-glusterfs-competitors-pulling-a-microsoft,
    /category/rants-amp-raves/are-glusterfs-competitors-pulling-a-microsoft]
categories: [GlusterFS, Rants & Raves]
date: '2012-08-18T00:46:16Z'
description: >
    'The kernel patch that nuked GlusterFS was signed off by mostly Lustre
    developers:'
draft: false
tags:
slug: are-glusterfs-competitors-pulling-a-microsoft
title: Are GlusterFS competitors pulling a Microsoft?

---

The [kernel patch](http://git.kernel.org/?p=linux/kernel/git/stable/linux-
stable.git;a=commit;h=d1f5273e9adb40724a85272f248f210dc4ce919a) that [nuked
GlusterFS](/blog/glusterfs-bit-by-ext4-structure-change/) was signed off by
mostly Lustre developers:

    
    
     Signed-off-by: Fan Yong <yong.fan@whamcloud.com>  
     Signed-off-by: Andreas Dilger <adilger@whamcloud.com>  
     Signed-off-by: Bernd Schubert <bernd.schubert@itwm.fraunhofer.de>  
     Signed-off-by: "Theodore Ts'o" <tytso@mit.edu>

This is a well known tactic that Microsoft (should probably throw in the word
"allegedly" here to avoid the legal ramifications of stating the certainty of
what everyone knew at the time) often took in the 80s to fight against the
more popular Lotus products, making changes to the operating system that would
break their competitors products whilst already having theirs prepared for the
change.

I'm not accusing Whamcloud of doing these unfair practices. Indeed, a little
bit of searching and I was able to find [this
thread](http://comments.gmane.org/gmane.linux.nfs/40863) which basically is
what I expected to find. They found a kernel deficiency that interfered with
what they were trying to do and offered a patch.

I'm just not so sure this should have been accepted so easily and backported
to stable kernels by Red Hat.

What do you think?

## ** Followup **

March 28, 2013 - I received an email today from Bernd Shubert concerned that
the tone of this post was a bit accusitory. Granted, I went for the
sensational title, but I hoped that the rest of the tone was fair and
balanced. My intention was to prompt discussion.

> Hello Joe,  
>  
> I just found your blog and honestly, I think what you accuse Whamcloud and
me off is rather unfair.  
> Firstly, if you look more deeper into the history of this patch, you will
figure out that Fan Yong first commited it, but then I heavily updated it. The
commit that landed was not very close to Fang Yongs original patch, I only
left Fang Yongs name as author, as he started this work.  
> Secondly, please check what Gluster (and as I just noticed Samba probably as
well) is doing - they take a long value and bit-shift it. Nothing ever defined
one could do that. What did you expect me to do when I worked  on this patch?
Check all software on this planet if it is posix compliant? Please also note
that I used my working address for this patch and not my private address. I
don't think I'm allowed to test gluster on our development systems to improve
it...  
>  
> Thirdly, yes, I'm working on FhGFS, which is somehow also competing with
Gluster and yes, I have worked before on Lustre and yes, I'm still in contact
with Lustre developers (and yes, we are definitely competing with Lustre). But
no, I never intended to break any other software with this patch. My primary
intentation with this patch was to fix a serious issue we have with ext4/ext3
- readdir() for large directories did not work properly due to hash
collisions. Lustre and any other software that needs reliable directory
offsets has the same issue of course. If you are proposing different and
better interfaces we could use, I'm happily to work on any patches. Posix is
rather limitating for FhGFS sometimes, Lustre sometimes has an advantage in
that area, as they bring kernel patches and can define theirs own interfaces
(although that makes it a pain with new kernel versions).  
>  
> Best regards,  
> Bernd

