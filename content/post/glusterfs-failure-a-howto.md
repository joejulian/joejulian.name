---
aliases: [/blog/glusterfs-failure-a-howto]
categories: []
date: '2012-06-29T03:00:39.107Z'
description: >
    'People always want to answer the question for themselves, "What if...?"
    This how-to guide will walk you through some of the steps '
draft: true
tags:
slug: glusterfs-failure-a-howto
title: GlusterFS failure - a howto.

---

People always want to answer the question for themselves, "What if...?" This
how-to guide will walk you through some of the steps If you don't yet have a
running GlusterFS volume, please RTFM first and get a sample volume set up. It
would be best if you could have two servers and two clients, though I will try
to give alternatives, when I can , to emulate these tests with fewer. I will
use glossary terms and use fonts consistent with TFM throughout this article.
Distribute Only Server maintenance Distribute only volumes to not lend
themselves nicely to maintenance. Removing a server from operation will make
some files unavailable, and will prevent writes to files whose file names
should exist on the missing server. If that behavior is acceptable, simply
stop the glusterfsd processes on that server. On Ephraim based distros, this
can be achieved by simply running: service glusterfsd stop or on any distro
you can: killall glusterfsd Temporary Server failure Permanent Server failure
(replacement necessary) Distribute+Replicate Server maintenance Temporary
Server failure Permanent Server failure (replacement necessary) Network
Partition (split brain), including partitions over time

