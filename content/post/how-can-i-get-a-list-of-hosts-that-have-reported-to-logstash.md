---
aliases: [/blog/how-can-i-get-a-list-of-hosts-that-have-reported-to-logstash, /category/howtos/how-can-i-get-a-list-of-hosts-that-have-reported-to-logstash,
    /category/logstash/how-can-i-get-a-list-of-hosts-that-have-reported-to-logstash]
categories: [Howtos, Logstash]
date: '2013-02-09T02:35:12Z'
description: >
    So I set my puppet script to install beaver on a set of machines and
    feed the system logs to logstash. Now some of them have fairly infrequent log
    entries so I just wanted to get a list of all the loghosts in logstash.
draft: false
tags:
    - logstash
    - elasticsearch
    - howto
slug: how-can-i-get-a-list-of-hosts-that-have-reported-to-logstash
title: How can I get a list of hosts that have reported to logstash?

---

So I set my puppet script to install beaver on a set of machines and feed the
system logs to logstash. Now some of them have fairly infrequent log entries
so I just wanted to get a list of all the loghosts in logstash.

The solution is to query ElasticSearch directly:

    
    
    curl -X POST "http://logstash:9200/_search?pretty=true" -d '{  
     "facets" : {  
     "tags": {"terms": {"field" : "logsource"} }  
     }'
    
    
    {
      "took" : 9,
      "timed_out" : false,
      "_shards" : {
        "total" : 25,
        "successful" : 25,
        "failed" : 0
      },
      "hits" : {
        "total" : 29182,
    ...
      "facets" : {
        "tags" : {
          "_type" : "terms",
          "missing" : 8579,
          "total" : 20607,
          "other" : 0,
          "terms" : [ {
            "term" : "alex",
            "count" : 11891
          }, {
            "term" : "belinda",
            "count" : 4603
          }, {
            "term" : "chuck",
            "count" : 4113
          } ]
        }
      }
    }

Not exactly what I was hoping for. Now to find out where those other servers
are.

