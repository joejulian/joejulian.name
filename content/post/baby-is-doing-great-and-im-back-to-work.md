---
aliases: [/blog/baby-is-doing-great-and-im-back-to-work]
categories: []
date: '2010-04-16T03:59:13Z'
description: >
    ''
draft: false
tags:
    - Google
    - Maps
    - Tricia
    - LocationFinder
    - component
    - Project
    - Joomla!
    - Plugins
    - Baby
    - Joomla!
slug: baby-is-doing-great-and-im-back-to-work
title: Baby is doing great, and I'm back to work!

---

![My Little Newborn Daughter](http://joejulian.name/wp-content/uploads/2010/04
/thinker-300x200.jpg)

My daughter was born on March 19th 2010, 7 lbz 5 oz (3.3 kg), 19 1/4 inches
(49 cm) and healthy and happy.

I'm back to work now and making progress on LocationFinder again. Be sure to
check the [project](http://joejulian.name/project/) page for development
details!

