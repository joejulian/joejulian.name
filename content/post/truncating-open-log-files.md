---
aliases: [/blog/truncating-open-log-files, /category/howtos/truncating-open-log-files]
categories: [Howtos]
date: '2016-10-19T23:53:44Z'
description: >
    There are a number of applications that open a log file and just keep
    writing to it. This is, of course, a problem when you rotate logs as the application
    just keeps writing to the renamed log instead of opening the new one.
draft: false
tags:
slug: truncating-open-log-files
title: Truncating open log files

---

There are a number of applications that open a log file and just keep writing
to it. This is, of course, a problem when you rotate logs as the application
just keeps writing to the renamed log instead of opening the new one.

In logrotate you can handle this with the `copytruncate` option. It will copy
the contents of the log to a new file and truncate the old one. Your
application will merrily continue appending to the newly 0-sized log.

If you want to do that manually, consider the truncate command:

    
    
    truncate --size=0 /var/log/foo.log
    

