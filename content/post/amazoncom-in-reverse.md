---
aliases: [/blog/amazoncom-in-reverse]
categories: []
date: '2012-01-16T07:05:07Z'
description: >
    Amazon has built a huge business listing everything imaginable. They
    have successfully built a brand where you know you can go to find anything. It's
    a good move for what Bezos intended.
draft: false
tags:
slug: amazoncom-in-reverse
title: Amazon.com In Reverse

---

Amazon has built a huge business listing everything imaginable. They have
successfully built a brand where you know you can go to find anything. It's a
good move for what Bezos intended.

At Ed Wyse, I'm building the opposite.

Rather than bringing in small retailers to round out our product list and then
competing with them directly as Amazon's been doing lately, we're hoping to be
the fulfillment resource for hair salons, nail salons, spas, massage clinics,
etc. that don't have time to manage a web store. They don't want to handle the
payments, inventory, and/or shipping, they just want their customers taken
care of.

To that end, I've been [developing an API](http://trac.edwyse.com/ "Ed Wyse
API development") to allow our customers to host their own stores, branded
their own way, with our warehouse and payment systems supporting them.

This has been a very large project. The original database structure was
unmanageable. We have a challenge of keeping another piece of (cobol) software
in-sync with our web site. And I have all my regular day-to-day issues that
need to be resolved. It's coming together though.

I've chosen [django](https://www.djangoproject.com/ "Django is a high-level
Python Web framework that encourages rapid development and clean, pragmatic
design.") as the framework for a new web site. This allows me to build
sensible models and build an API for them using
[Tastypie](https://github.com/toastdriven/django-tastypie).

Over the next several weeks, various partf of the API will be made available
to our wholesale customers. Check your account page for your UUID and API key.
Once you have those, give them to your web admin and point him to our API
development page, [http://trac.edwyse.com](http://trac.edwyse.com/). This will
have some boilerplate pages that you can use as well as documentation for
creating your own design.

