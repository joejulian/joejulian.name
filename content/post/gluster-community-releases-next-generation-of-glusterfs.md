---
aliases: [/blog/gluster-community-releases-next-generation-of-glusterfs, /category/glusterfs/gluster-community-releases-next-generation-of-glusterfs]
categories: [GlusterFS]
date: '2012-05-31T13:53:12.374Z'
description: >
    "The release of GlusterFS 3.3.0 by the Gluster Community marks a major\
    \ milestone in Clustered File Storage. GlusterFS is the leading open source solution\
    \ for the dramatically increasing volume of unstructured data. It is a software-only,\
    \ highly available, scale-out, centrally managed storage pool that can be backed\
    \ by any POSIX filesystem that supports extended attributes, such as Ext3/4, XFS,\
    \ BTRFS and many more.As an example of Red Hat's goal of building strong, independent,\
    \ open source communities, GlusterFS 3.3.0 marks the first release as an \u201C\
    upstream\u201D project with its own release schedule. This release addresses many\
    \ of the most commonly requested features including proactive self-healing, quorum\
    \ enforcement, and granular locking for self-healing, as well as many more bug\
    \ fixes and enhancements.Some of the more noteworthy features include:"
draft: false
tags:
slug: gluster-community-releases-next-generation-of-glusterfs
title: Gluster Community releases next generation of GlusterFS

---

The release of GlusterFS 3.3.0 by the [Gluster Community](http://gluster.org)
marks a major milestone in Clustered File Storage. GlusterFS is the leading
open source solution for the dramatically increasing volume of unstructured
data. It is a software-only, highly available, scale-out, centrally managed
storage pool that can be backed by any POSIX filesystem that supports extended
attributes, such as Ext3/4, XFS, BTRFS and many more.  
  
As an example of Red Hat's goal of building strong, independent, open source
communities, GlusterFS 3.3.0 marks the first release as an “upstream” project
with its own release schedule. This release addresses many of the most
commonly requested features including proactive self-healing, quorum
enforcement, and granular locking for self-healing, as well as many more bug
fixes and enhancements.  
  
Some of the more noteworthy features include:

  * Unified File and Object storage - Blending [OpenStack’s Object Storage API ](http://openstack.org/projects/storage/) with GlusterFS provides simultaneous read and write access to data as files or as objects.
  * HDFS compatibility - Gives Hadoop admins the ability to run map/reduce jobs on unstructured data on GlusterFS and access the data with well-known tools and shell scripts.
  * Proactive self-healing - GlusterFS volumes will now automatically restore file integrity after a replica recovers from failure.
  * Granular locking - Allows large files to be accessed even during self-healing, a feature that is particularly important for VM images.
  * Replication improvements - with quorum enforcement you are assured that  your data has been written in at least the configured number of places before the file operation returns, allowing a user-configurable adjustment to fault tolerance vs performance.

  
Visit [Gluster.org](http://gluster.org) to download. Packages are available
for most distributions, including Ubuntu, Debian, Fedora, RHEL, and CentOS.  
  
Get involved! Join us on #gluster on freenode, join our [mailing
list](http://www.gluster.org/interact/mailinglists/), ‘like’ our [Facebook
page](http://facebook.com/GlusterInc), follow us on
[twitter](http://twitter.com/glusterorg), or check out our [LinkedIn
group](http://www.linkedin.com/groups?gid=99784).  
  
GlusterFS is an open source project sponsored by [Red
Hat](http://www.redhat.com/)®, who uses it in its line of [Red Hat
Storage](http://www.redhat.com/storage/) products.

