---
aliases: [/blog/what-i-want-to-do-with-openstack, /category/openstack/what-i-want-to-do-with-openstack]
categories: [OpenStack]
date: '2012-08-14T21:48:10Z'
description: >
    I'm trying to use openstack for my 2 vm hosts. I think that this will
    puppetize better than how I'm doing it now. Primarily, I think OpenStack will
    offer more flexibility when I need to schedule hardware maintenance as it will
    handle which compute node the vm goes on so puppet won't end up restarting the
    VM I just migrated to the other host. Using simple nova cli commands, puppet can
    inquire as to the states of machines and manage them without being so node dependent.
draft: false
tags:
    - centos
    - openstack
    - kvm
    - libvirt
    - network
    - virtualization
slug: what-i-want-to-do-with-openstack
title: What I want to do with OpenStack

---

I'm trying to use openstack for my 2 vm hosts. I think that this will
puppetize better than [how I'm doing it now](/blog/how-i-do-vm-management-
using-puppet-kvm-and-glusterfs-on-rhelcentos/). Primarily, I think OpenStack
will offer more flexibility when I need to schedule hardware maintenance as it
will handle which compute node the vm goes on so puppet won't end up
restarting the VM I just migrated to the other host. Using simple nova cli
commands, puppet can inquire as to the states of machines and manage them
without being so node dependent.

The main problem I'm having is with the network configuration. I have an ISC
DHCP server configuration that has been working quite well to dish out
addresses over our entire enterprise. Over time, we have built several
features on top of this so we would really like to continue using our existing
DHCP service for the public side of our virtual machines.

![Map of the network design I would
like](/media/uploads/images/openstack_network_requirement.png)

This doesn't work, of course, because OpenStack has no idea what address ISC
assigned. This prevents it from setting the filters properly so the VM can't
communicate on the network.

For the private side, I couldn't care less by what method it's configured.
It's on it's own bridge, nic, and switch.

Is there a way to do what I want without just stripping the filters out of the
libvirt template?

