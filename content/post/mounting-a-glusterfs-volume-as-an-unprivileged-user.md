---
aliases: [/blog/mounting-a-glusterfs-volume-as-an-unprivileged-user, /category/glusterfs/mounting-a-glusterfs-volume-as-an-unprivileged-user,
    /category/howtos/mounting-a-glusterfs-volume-as-an-unprivileged-user]
categories: [GlusterFS, Howtos]
date: '2012-04-18T17:29:10Z'
description: >
    Since GlusterFS is fuse based, it can be mounted as a standard user without
    too much difficulty.
draft: false
tags:
slug: mounting-a-glusterfs-volume-as-an-unprivileged-user
title: Mounting a GlusterFS volume as an unprivileged user

---

Since GlusterFS is fuse based, it can be mounted as a standard user without
too much difficulty.

On a server:

` gluster volume set $VOLUME allow-insecure on `

On the client as root:

` echo user_allow_other >> /etc/fuse.conf `

To mount the volume, you can:

` /usr/sbin/glusterfs --log-level=INFO --log-
file=my_user_accessable_logfile.log --volfile-id=$VOLUME --volfile-
server=$SERVER $MOUNTPOINT `

Since mount.glusterfs is just a bash script, you should be able to alter it
pretty easily to be used using the standard mount command.

