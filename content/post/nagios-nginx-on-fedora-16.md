---
aliases: [/blog/nagios-nginx-on-fedora-16, /category/howtos/nagios-nginx-on-fedora-16]
categories: [Howtos]
date: '2012-02-23T23:40:37.083Z'
description: >
    Every howto in existence seems to be for non-rpm based distros. Maybe
    it's just because this is so easy, but here's the answer to that.
draft: true
tags:
    - howto
    - nginx
    - nagios
    - fedora
slug: nagios-nginx-on-fedora-16
title: Nagios + Nginx on Fedora 16

---

Every howto in existence seems to be for non-rpm based distros. Maybe it's
just because this is so easy, but here's the answer to that.

Install the packages:

`yum install nagios nginx spawn-fcgi`

Something else goes here

