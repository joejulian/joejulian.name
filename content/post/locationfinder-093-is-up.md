---
aliases: [/blog/locationfinder-093-is-up]
categories: []
date: '2010-02-07T17:40:57Z'
description: >
    That's embarrassing. I told myself the last time that I need to install
    the release myself before posting, and with a dozen people in the house for a
    baby shower and wanting to meet my deadline, I popped into my office and zipped
    up the build tree and did it again.
draft: false
tags:
    - Google
    - Maps
    - testing
    - component
    - Location Finder
    - Release
    - Joomla!
    - Plugins
slug: locationfinder-093-is-up
title: LocationFinder 0.9.3 is up

---

That's embarrassing. I told myself the last time that I need to install the
release myself before posting, and with a dozen people in the house for a baby
shower and wanting to meet my deadline, I popped into my office and zipped up
the build tree and did it again.

Sorry about that.

If you downloaded 0.9.2 it won't even install.

Please install 0.9.3 instead and, as always, [look here for
changes](http://joomla.joejulian.name/bzr/projects/locationfinder/repository)

[dm]15[/dm]

