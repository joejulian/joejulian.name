---
aliases: [/blog/introduction-to-glusterfs-at-sasag-nov-8]
categories: []
date: '2012-11-03T22:48:00Z'
description: >
    If you're in the Seattle area and would like to meet up and hear me talk
    about GlusterFS, come to the Seattle Area System Administrator's Guild meeting
    7:00pm November 8th at the University of Washington (Bldg EE1, Rm 403). There
    will be dinner sponsored by Silicon Mechanics so even if I suck, it won't be a
    complete waste of time.
draft: false
tags:
    - glusterfs
    - sasag
    - presentations
slug: introduction-to-glusterfs-at-sasag-nov-8
title: Introduction to GlusterFS at SASAG Nov 8

---

If you're in the Seattle area and would like to meet up and [hear me talk
about GlusterFS](http://www.sasag.org/2012/10/31/nov-8th-meeting-an-
introduction-to-glusterfs/), come to the [Seattle Area System Administrator's
Guild ](http://www.sasag.org/)meeting 7:00pm November 8th at the University of
Washington ([Bldg EE1, Rm
403](http://www.ee.washington.edu/about/contact.html)). There will be dinner
sponsored by Silicon Mechanics so even if I suck, it won't be a complete waste
of time.

