---
aliases: [/blog/flawed-pci-compliance-testing, /category/rants-amp-raves/flawed-pci-compliance-testing]
categories: [Rants & Raves]
date: '2012-06-08T22:18:50Z'
description: >
    Security Metrics is a company that we use to test for PCI security complience.
    Occasionally they add new tests to their battery, and they're not always tested
    very well. Today I received notice that we did not pass due to CSS testing. That
    testing was a complete and utter failure on their part to write a valid test.
draft: false
tags:
    - apache
    - pci
slug: flawed-pci-compliance-testing
title: Flawed PCI Compliance testing.

---

Security Metrics is a company that we use to test for PCI security complience.
Occasionally they add new tests to their battery, and they're not always
tested very well. Today I received notice that we did not pass due to CSS
testing. That testing was a complete and utter failure on their part to write
a valid test.

![](/media/uploads/images/pci_compliance_test_fail.png)

It turns out that what they were complaining about was that the script was
returned in the query they performed against several pages, such as
<http://www.edwyse.com/form_members.asp?find=%22;}alert(9823);function%20x(){v%20=%22>.
Those queries were returning 301s as we redirect all http queries to https.
The standard 301 response from apache produces html that includes the injected
script:

    
    
    <!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
    <html><head>
    <title>301 Moved Permanently</title>
    </head><body>
    <h1>Moved Permanently</h1>
    <p>The document has moved <a href="https://www.edwyse.com/form_members.asp?find=%22;}alert(9823);function%20x(){v%20=%22">here</a>.</p>
    </body></html>

Since the response was a 301 with a "Location" header, they should have
ignored any body.

Rather than acknowledge that and fix their problem, they start by demanding
that I waste my time proving that we sanitize the output from all our scripts.

That's not going to happen. They need to fix their flawed testing procedures.  
  

