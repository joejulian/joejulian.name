---
aliases: [/blog/software-defined-storage-and-open-what-the-heck-are-they, /category/rants-amp-raves/software-defined-storage-and-open-what-the-heck-are-they]
categories: [Rants & Raves]
date: '2013-10-22T17:01:23.547Z'
description: >
    A few weeks ago, "Software Defined Storage" and "Open" were all the news
    in the "cloud" industry as EMC announced they had an "Open" "Software Defined
    Storage" solution. I heard the news and rolled my eyes. Yeah, right. ... but I
    was busy with real life things and didn't even have time to read the announcement,
    much less any of the buzz around it.
draft: false
tags:
slug: software-defined-storage-and-open-what-the-heck-are-they
title: '"Software Defined Storage" and "Open": What the heck are they?'

---

A few weeks ago, "Software Defined Storage" and "Open" were all the news in
the "cloud" industry as EMC announced they had an "Open" "Software Defined
Storage" solution. I heard the news and rolled my eyes. Yeah, right. ... but I
was busy with real life things and didn't even have time to read the
announcement, much less any of the buzz around it.

Until today...

I've read all the buzzwords and hype, and still I'm not entirely sure what
they're offering. One keyword that's missing from everything is "POSIX" an
acronym for "Portable Operating System Interface" which is an IEEE standard
that is an open, operating system independant standard api for filesystem
interaction. It does appear that they're offering yet another object based
api, nfs, cifs, and iSCSI as well as S3 and Swift compatibility, but it's
unclear from their documentation as to whether those are north facing or just
south.

What is "Software Defined Storage"?

Enterprise Management Associates, Inc., whose paper reads as if they are a
paid shill for EMC would like SDS to be defined in such a way that their
definition aligns almost perfectly with EMC's product announcement
(http://www.emc.com/collateral/analyst-reports/ema-emc-vipr.pdf), even
Wikipedia seems to have followed the commercial shill lines and left true
software defined storage at the bottom of a bullet list.

Software defined storage is any storage where the logic that defines that
storage is abstracted into a software layer. Using that same software layer,
therefore, it should be possible to define the physical storage in multiple
ways. SDS is a tool or set of tools that allows you to use software to design
a storage system that best fits your use case.

What is "Open"?

When I first got in to the computer industry, there were software producers
and consumers and there was a huge paywall between the two. As a consumer of
software, there was nothing open about it. Some features were documented, but
most were not. File structures, interfaces, memory models, even the user
interfaces were proprietary and would be changed between releases to ensure
that any competitor's products that had managed to decypher them and
integrated their product would break. Big software vendors really had no
interest in a little beauty supply distributor in just three states. Their
focus was on the Fortune 500 and if you wanted to report a bug, you were
welcome to, but it wasn't likely to have any resources applied to it unless
you were someone like Boeing.

Today, with open source, anyone can become involved in the production cycle,
even without programming skills. I hang out on IRC and, in my spare time, help
people understand GlusterFS. I look at the industry trends and communicate
directly with the developers that produce the code. Together we brainstorm
over new ideas or bugs. We, the consumers and the producers, are all part of a
community. That's not just some buzzword but it's actual interaction. Some of
the developers have become my friends. This is "open".

"Open" is a breakdown of the barrier between the producers and the consumers.
"Open" is where the person (or company) with the problem to solve is involved
in defining the solution to that problem. That's true for open source software
and also open standards.

One company, be it EMC, Microsoft, VMWare, Amazon, etc., telling you what's an
"open" solution, is not open. That's proprietary by it's very nature.

EMC's product has a pretty GUI, it may indeed be software defined storage. It
is not, however, "open".

