---
aliases: [/blog/location-finder-delays-due-to-baby]
categories: []
date: '2010-03-10T15:16:03Z'
description: >
    Thought I thought I was going to be able to continue working at my previous
    pace, however, as we approach March 19th I found my wife has several things that
    she needs to have done before the baby arrives. This has pushed back my schedule
    dramatically. I will be getting back to the Location Finder project shortly afterward
    though.
draft: false
tags:
    - Joomla!
    - Plugins
slug: location-finder-delays-due-to-baby
title: Location Finder delays due to Baby

---

Thought I thought I was going to be able to continue working at my previous
pace, however, as we approach March 19th I found my wife has several things
that she _needs_ to have done before the baby arrives. This has pushed back my
schedule dramatically. I will be getting back to the Location Finder project
shortly afterward though.

Thanks for your patience.

