---
aliases: [/blog/clickexplorer-interferes-with-locationfinder]
categories: []
date: '2010-02-17T03:17:26Z'
description: >
    I've discovered that clickExplorer, a javascript plugin that claims to
    protects your content, interferes with the submission clicks necessary to retrieve
    the location markers.
draft: false
tags:
    - Joomla!
    - Plugins
slug: clickexplorer-interferes-with-locationfinder
title: ClickExplorer Interferes With LocationFinder

---

I've discovered that clickExplorer, a javascript plugin that claims to
protects your content, interferes with the submission clicks necessary to
retrieve the location markers.

It doesn't really help anyway. Anybody who wants to steal your content can do
so by viewing the source, downloading the page, etc. Additionally, it adds the
annoying feature of making it impossible to right-click paste into forms.

If you want to use LocationFinder, you'll have to disable clickExplorer.

