#!/bin/env python
#
# (C) 2011, Joe Julian 
#
# License: GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
#

import os,socket,xattr,sys,time
from stat import *

def scandirs(arg, dirname, fnames):
    for fn in fnames:
        filename = os.path.join(dirname,fn)
        fmode = os.lstat(filename).st_mode
	if S_ISREG(fmode) and os.path.getsize(filename) > 0:
            try:
                attributes = xattr.listxattr(filename)
                status = [ xattr.getxattr(filename,x) for x in attributes if x.startswith("trusted.afr.") ]
                if len([ x for x in status if not x == '\x00'*12]) > 0:
                    if S_IMODE(fmode) == 512:
                        os.remove(filename)
                        status = []
                    else:
                        time.sleep(0.2)
                        status = [ xattr.getxattr(filename,x) for x in attributes if x.startswith("trusted.afr.") ]
                if len([ x for x in status if not x == '\x00'*12]) > 0:
                    for x in attributes: 
                        if x.startswith("trusted.afr") and not xattr.getxattr(filename,x) == '\x00'*12:
                            print filename
                            print "\t%s=0x%s" % (x,"".join(["%02x" % ord(y) for y in list(xattr.getxattr(filename,x))]))
            except:
                status = ""

if not len(sys.argv) == 2:
    print """Usage:
        %s {brick directory}
    """ % sys.argv[0]
else:
    os.path.walk(sys.argv[1],scandirs, None)
