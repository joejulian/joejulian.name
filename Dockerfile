FROM ruby:2.4-alpine

RUN apk add --update --no-cache --virtual build-dependencies

RUN apk add --update --no-cache \
		bash \
		build-base \
		ca-certificates \
		curl \
		git \
		libcurl \
		libxml2-dev \
		libxslt-dev \
        nodejs \
		openssh \
		rsync \
		wget
RUN gem install \
		html-proofer \
		nokogiri

ENV HUGO_VERSION=0.40
ENV HUGO_URL=https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/hugo_${HUGO_VERSION}_Linux-64bit.tar.gz
RUN wget -O - ${HUGO_URL} | tar xzv -C /usr/bin hugo

COPY ./ /work/

WORKDIR /work

RUN npm install hugo-lunr

RUN hugo 
RUN npm run index || cat /work/npm-debug.log
RUN htmlproofer --internal-domains joejulian.name public || true # Don't block on this yet

FROM nginx:alpine

COPY --from=0 /work/public /usr/share/nginx/html
COPY nginx  /etc/nginx/conf.d
